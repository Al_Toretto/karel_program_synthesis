#!/bin/bash
line_count=$(($(wc -l karel-dataset/1m_6ex_karel/train.json | tr -d -c 0-9) + 1))
one_percent=$(($line_count/100))
three_percent=$(($line_count*3/100))
ten_percent=$(($line_count*10/100))

head karel-dataset/1m_6ex_karel/train.json -n $one_percent > train_one_percent.json
head karel-dataset/1m_6ex_karel/train.json -n $three_percent > train_three_percent.json
head karel-dataset/1m_6ex_karel/train.json-n $one_percent > train_ten_percent.json
