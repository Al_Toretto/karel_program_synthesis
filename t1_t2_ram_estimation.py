import json
from pympler.asizeof import asizeof

examples = []
for line in open('train_one_percent.json', 'r'):
    examples.append(json.loads(line))

usage_in_MB = asizeof(examples)/1024/1024
print(usage_in_MB, " MB")
print("RAM usage of the full train.json: ", 100*usage_in_MB/1024, " GB")
