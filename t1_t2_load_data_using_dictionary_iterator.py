import json
from pympler.asizeof import asizeof

def CreateLoadGenerator():
    for line in open('karel-dataset/1m_6ex_karel/train.json', 'r'):
        yield json.loads(line)

load_generator = CreateLoadGenerator()

num = 0
for json_object in load_generator:
    print(num)
    num += 1
